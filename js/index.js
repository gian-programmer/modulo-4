$(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
            interval:1500
        })

        $('#contacto').on("show.bs.modal", function(e){
            console.log("el modal se esta mostrando");
            $('#contactobtn').removeClass('btn-success');
            $('#contactobtn').addClass('btn-danger');
            $('#contactobtn').prop('disabled',true);
        });

        $('#contacto').on("shown.bs.modal", function(e){
            console.log("el modal se mostro,cambio de color y se inhabilito el boton");
        });

        $('#contacto').on("hide.bs.modal", function(e){
            console.log("el modal se esta ocultando");
        });

        $('#contacto').on("hidden.bs.modal", function(e){
        console.log("el modal se oculto ,volvio a su color original y se habilito el boton");
        $('#contactobtn').removeClass('btn-danger');
        $('#contactobtn').addClass('btn-success');
        $('#contactobtn').prop('disabled',false);
        });
    
});